const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, "First name is required"]
    },
    lastName: {
        type: String,
        required: [true, "Last name is required"]
    },
    email: {
        type: String,
        required: [true, "Your email is required"]
    },
    password: {
        type: String,
        required: [true, "Input a password is a must"]
    },    
    isAdmin: {
        type: Boolean,
        default: false
    },
    mobileNo: {
        type: String,
        required: [true, "You are required to give your contact#"]
    },    
    enrollments: [
        
        {
            courseId: {
                type: String,
                required: [true, "courseID is required"]
            },
            enrolledOn: {
                type: Date,
                default: new Date()
            },
            status: {
                type: String,
                default: "Enrolled"
            }
        }
    ]
});

module.exports = mongoose.model("User", userSchema);