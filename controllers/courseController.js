const Course = require("../models/Course");


// Controller Functions:

// Creating a new course
module.exports.addCourse = (userData, reqBody) => {

        let newCourse = new Course({
            name: reqBody.name,
            description: reqBody.description,
            price: reqBody.price
        });

        return newCourse.save().then((course, error) => {
            if(error) {
                return false //"Course creation failed"
            } else {
                return true
            }
        })
}

//Retrieving All Courses
module.exports.getAllCourses = (data) => {

    if(data.isAdmin) {
        return Course.find({}).then(result => {
            
            return result
        })
    } else {
        return false // "You are not an Admin."
    }
};


// Retrieving All Active Courses
module.exports.getAllActive = () => {
    
    return Course.find({isActive: true}).then(result => {
        return result
    })
}

// Retrieve a specific course
module.exports.getCourse = (reqParams) => {

    return Course.findById(reqParams.courseId).then(result => {
        
        return result
    })
}


// Update a specific course
module.exports.updateCourse = (reqParams, reqBody) => {

    let updatedCourse = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    }

    return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((updatedCourse, error) => {

        console.log(updatedCourse)
        if(error) {
            return false
        } else {
            return true
        }
    })
};

// Archive a course
module.exports.archiveCourse = (reqParams, reqBody) => {
    
    let archivedCourse = {
        isActive: reqBody.isActive
    }

    return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((archivedCourse, error)=>{
        if(error){
            return false
        } else {
            return true
        }
    })
};

// module.exports.archiveCourse = async (reqParams, reqBody) => {
//     try {
//         let archivedCourse = {
//             isActive: reqBody.isActive
//         }
    
//         await Course.findByIdAndUpdate(reqParams.courseId, archivedCourse)
//         return true
//     } catch(error) {
//         return false
//     }
// };